ARG BASE_TOOLCHAIN_REGISTRY_IMAGE=registry.gitlab.com/automated-linux-from-scratch/base-toolchain
ARG BASE_TOOLCHAIN_TAG=master
FROM ${BASE_TOOLCHAIN_REGISTRY_IMAGE}:${BASE_TOOLCHAIN_TAG} as builder
ARG CPUS=1
ENV ANSIBLE_CONFIG=/ansible/ansible.cfg
ENV ANSIBLE_FORCE_COLOR=1
ENV NPROC=${CPUS}
COPY . /ansible
RUN ansible-playbook /ansible/00_build_toolchain.yml --diff

FROM scratch as toolchain
LABEL maintainer="Aleksander Ciołek <aleksander.ciolek@protonmail.com>"
COPY --from=builder /build/sources /sources/
COPY --from=builder /build/tools /tools/
ENV PATH=/tools/bin
CMD ["/tools/bin/bash"]
