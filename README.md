# Overview

Build the LFS toolchain. This is the second step in LFS pipeline and covers 
[chapter 5 of LFS book](http://www.linuxfromscratch.org/lfs/view/9.1-systemd/chapter05/introduction.html).

Refer to the `docs` project for a general overview of the whole `automated-linux-from-scratch` group.

# Build

Execute `docker build` and overwrite build arguments when applicable. 

For example:
```shell script
$ export BASE_TOOLCHAIN_REGISTRY_IMAGE=base-toolchain
$ export BASE_TOOLCHAIN_TAG=dev
$ export CPUS=4
$ docker build -t toolchain:dev -f Dockerfile \
    --build-arg BASE_TOOLCHAIN_REGISTRY_IMAGE \
    --build-arg BASE_TOOLCHAIN_TAG \
    --build-arg CPUS \
    ansible
```

will have the following effects:
- local `base-toolchain:dev` image used as a builder image,
- `4 CPUs` used during compilation,

and produce an image `toolchain:dev`.
